<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Fwz/Mbp2O55I3TKlypYMpvbLM+uR5q3U/aAdOWNivQ2xySEcL880W7/oEQl6mOZffFzSmff4xUbh7bDGLzpVlQ==');
define('SECURE_AUTH_KEY',  'Rv7JUq3LYfEF9e07tLc1ymyEvK13OOA1yecr7s6/QuAGts20HYnmcxEMVAGvn283iMQmGpdxl0eHh2PNNvdlFA==');
define('LOGGED_IN_KEY',    'gId4FSDuxD22tzuRjWneB4j3GFMrETYMrgPcucKcvMlTzySymyLaT88thj9CBS9Xv+TXqB9+Gfo2Bi5jVAmvUA==');
define('NONCE_KEY',        'MyjWNHlMU5RfmB2p7vZRPIwnbQzifKTLN7UGMbZ3TQ11sC3pyvJxdRxc8TPvh4wHPsAbON8miGPUN1FGcQYDiw==');
define('AUTH_SALT',        'pzR7EreKMZCVmPXKmqPGL3vEk/+B0D85S8d/p1kzzyblLedXV+2oQFqs1gmbyV9Am/1YbL8kOck3k4ivaNvlzg==');
define('SECURE_AUTH_SALT', 'pkOGARJ5Uf7Pp2oAubOkiv9fXwQAH+iWHYFu5TpJf+U8ndk4RLxqVp/JmA4JnuJVdVWTtpUdjmRipd2xQ0OQDw==');
define('LOGGED_IN_SALT',   'BIIyk1YErDlsCy5k9UXETMoKR31+vDLgW/TIytge90K4jNZVo6cv7s+BLf2ytlpN62AymdfOJfE0q3qBGJlT4g==');
define('NONCE_SALT',       'sfcWoRgOWLWCf2bPxUQg8kAzxyQkfaiHp/CjottVBYpuOIUXje6aDoCgCLYvi6/8L7JveBN4COt/bzez1YGUsA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
